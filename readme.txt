=== Ajax Controller ===
Contributors: angusdowling
Tags: wordpress, tinymce
Requires at least: 4.7
Tested up to: 4.8.2
Stable tag: 4.9
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Easily add a custom button to TinyMCE editor

== Description ==

Insert description here

= Docs & Support =

Insert docs & support here

== Installation ==

Insert installation instructions here