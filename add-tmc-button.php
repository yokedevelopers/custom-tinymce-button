<?php
/**
 * Plugin Name: Add TinyMCE Button
 * Description: Easily add TinyMCE Button
 * Version: 0.1.0
 * Author: Yoke
 *
 *
 * @package AddTmcButton
 * @category Core
 * @author Yoke
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'AddTmcButton' ) ) :

final class AddTmcButton {
	/**
	 * @var AddTmcButton The single instance of the class
	 */
	protected static $_instance = null;

	/**
	 * Main AC Instance
	 *
	 * Ensures only one instance of AC is loaded or can be loaded.
	 *
	 * @static
	 * @see AC()
	 * @return AddTmcButton - Main instance
	 */
	public static function instance()
	{

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;

	}

	private function __construct()
	{

		// Register actions
		add_action( 'admin_head', array($this, 'setupTinyMCEPlugins') );
		add_action( 'admin_enqueue_scripts', array($this, 'adminScriptsAndCSS') );

	}

	/**
	* Called on the admin_head action, adds filters to:
	* - register the TinyMCE Plugin Javascript,
	* - add a button to the TinyMCE Editor
	*/
	function setupTinyMCEPlugins() {
		
		add_filter( 'mce_external_plugins', array($this, 'addTinyMCEPlugin') );
		add_filter( 'mce_buttons', array($this, 'addTinyMCEButton') );

	}

	/**
	* Registers the TinyMCE Plugin Javascript
	*
	* @param array $plugins Plugins
	* @return array Plugins
	*/
	public function addTinyMCEPlugin( $plugins ) {
		
		$plugins['custom_tinymce_dashicons'] = plugins_url( 'js/script.js', __FILE__ );
		return $plugins;
		
	}

	/**
	* Adds a button to the TinyMCE Editor
	*
	* @param array $buttons Buttons
	* @return array Buttons
	*/
	public function addTinyMCEButton( $buttons ) {
		
		array_push( $buttons, 'custom_tinymce_dashicons' );
		return $buttons;
			
	}

	/**
	* Called on the admin_enqueue_scripts action, enqueues CSS to 
	* make all WordPress Dashicons available to TinyMCE. This is
	* where most of the magic happens.
	*/
	public function adminScriptsAndCSS() {
		
		wp_enqueue_style( 'custom_tinymce_dashicons', plugins_url( 'css/style.css', __FILE__ ) );
			
	}
}

endif;

/**
 * Returns the main instance of AC to prevent the need to use globals.
 *
 * @return AddTmcButton
 */
function ATB()
{
	return AddTmcButton::instance();
}

// Global for backwards compatibility.
$GLOBALS['AddTmcButton'] = ATB();